# Hubii � Front-end Engineer Exercise

## Challenge

For this exercise we want you to create a simple React app that visualises the price history of ETH/USD and BTC/USD. 

It should render the data in a readable, usable manner, and it should display the price history of one currency pair at the time. In addition it needs to be possible to choose which currency pair to show via the UI.

To get access to cryptocurrency price history you can register for a free plan at [coinapi.io](https://www.coinapi.io/pricing).

Relevant documentation:

[Latest data (time series)](https://docs.coinapi.io/#latest-data)

_Note that for this exersice it is sufficient to get data from one exchange, and there is no need for any weighted pricing based on multiple exchanges._


## Instructions

The code can be submitted with a public GitHub repo, or uploaded to a public Dropbox folder or similar. Please include any documenation and setup needed in order to run the app.

You can use any third party libraries in addition to React as you see fit. 


We ask that you NOT share this exercise with anyone outside of our interview process.

## Available Scripts

In the project directory, you can run:

### `npm install`

Will install all the dependencies required to run the application

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

