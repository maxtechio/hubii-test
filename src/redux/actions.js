
import * as CoinAPI from '../api/coinapi'

export const getCoinHistory = (symbol) => (dispatch, getState) => {

  const { data } = getState();

  dispatch(setSymbol(symbol))

  if (data[symbol])
    return dispatch(setCurrentList(data[symbol]))

  return CoinAPI.get(symbol)
        .then(payload => {
          if (Array.isArray(payload))
          {
            dispatch(setDataList(symbol, payload))
            dispatch(setCurrentList(payload))
          }
        })

}

export const SET_SYMBOL = "SET_SYMBOL";
export const setSymbol = (symbol) => {
  return { type: SET_SYMBOL, payload: { symbol }}
}

export const SET_DATA_LIST = "SET_DATA_LIST";
export const setDataList = (name, list) => {
  return { type: SET_DATA_LIST, payload: { name, list }}
}

export const SET_CURRENT_LIST = "SET_CURRENT_LIST";
export const setCurrentList = (list) => {
  return { type: SET_CURRENT_LIST, payload: { list }}
}