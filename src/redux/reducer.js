
import * as Actions from './actions'

const intialState = {
  symbol: "COINBASE_SPOT_BTC_USD",
  data: {},
  currentList: []
}

export default (state = intialState, action) => {
  switch(action.type){
    case Actions.SET_SYMBOL: {
      return { ...state, 
               symbol: action.payload.symbol,
               currentList: []
             }
    }
    case Actions.SET_DATA_LIST: {
      return { ...state, 
                data: {
                  ...state.data,
                  [action.payload.name]: action.payload.list
                } 
              }
    }
    case Actions.SET_CURRENT_LIST: {
      return { ...state, currentList: action.payload.list }
    }
    default: return state;
  }
}