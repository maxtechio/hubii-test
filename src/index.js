import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import CreateStore from './redux/store'
import App from './container/AppContainer';
import registerServiceWorker from './registerServiceWorker';

import './index.css';

ReactDOM.render(<Provider store={CreateStore()}>
  <App />
</Provider>, document.getElementById('root'));
registerServiceWorker();
