
import { connect } from 'react-redux'
import * as Actions from '../redux/actions'
import App from './App'

const mapStateToProps = (state) => {
  return {
    symbol: state.symbol,
    history: state.currentList
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCoinHistory: (symbol) => dispatch(Actions.getCoinHistory(symbol))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)