import React, { Component } from 'react';
import { Container, Navbar, NavbarBrand, Row, Col } from 'reactstrap';
import { Card, CardBody } from 'reactstrap';

import HistoryGrid from '../components/HistoryGrid'
import CoinButton from '../components/CoinButton'

import * as CoinAPI from '../api/coinapi'

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const coins = {
  ETH: {
    symbol: "COINBASE_SPOT_ETH_USD",
    name: "ETH"
  },
  BTC: {
    symbol: "COINBASE_SPOT_BTC_USD",
    name: "BTC"
  }
}

class App extends Component {

  constructor(props){
    super(props);

    this.props.getCoinHistory(this.props.symbol);
  }

  shouldComponentUpdate(nextProps){
    return true
  }

  onCoinClick(symbol){
    this.props.getCoinHistory(symbol);
  }

  render() {
    return (
      <div className="App">
        <Navbar color="dark" dark>
          <NavbarBrand href="/">hubii - test</NavbarBrand>
        </Navbar>

        <Container fluid={false}>

          <Row className="coin-selector">

            <Col xs={{ size: 4, offset: 2}} >
              <CoinButton 
                coin={coins.BTC.name}
                onClick={() => this.onCoinClick(coins.BTC.symbol)}
                active={this.props.symbol === coins.BTC.symbol}
              />
            </Col>

            <Col xs={{ size: 4}}>
              <CoinButton 
                coin={coins.ETH.name}
                onClick={() => this.onCoinClick(coins.ETH.symbol)}
                active={this.props.symbol === coins.ETH.symbol}
              />
            </Col>

          </Row>

          <Row>

            <Col xs={{ size: 12 }}>
              <Card>
                <CardBody>

                  <HistoryGrid history={this.props.history}/>

                </CardBody>
              </Card>
            </Col>

          </Row>

        </Container>
      </div>
    );
  }
}

export default App;
