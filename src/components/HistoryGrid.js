
import React from 'react';
import { Col, Row, ListGroup, ListGroupItem, Badge } from 'reactstrap';

const dateFormat = require('dateformat');

export default class HistoryGrid extends React.Component{

  groupByDate(){
    const dateGrouping = {};
    this.props.history.forEach((item, index) => {

      const itemDate = dateFormat(item.time_period_start, "yyyy-mm-dd", true)
      if (!dateGrouping[itemDate])
      {
        dateGrouping[itemDate] = {
          caption: dateFormat(item.time_period_start, "fullDate", true),
          items: [ item]
        }
      }else
        dateGrouping[itemDate].items.push(item);

    })

    return dateGrouping;
  }

  upOrDown(current, previous, key){
    if (!previous)
      return "same";
    if (current[key] > previous[key])
      return "up";
    if (current[key] < previous[key])
      return "down";
    return "same";
  }

  render(){

    const groupedByDate = this.groupByDate();

    return Object.keys(groupedByDate)
            .map((date, index) => {
              const dateObj = groupedByDate[date];
              
              return (
                <ListGroup key={index}>
                  <h4>{dateObj.caption}</h4>
                  <ListGroupItem>
                    <Row>
                      <Col xs="2"><Badge>Time End</Badge></Col>
                      <Col xs="2"><Badge>Price End</Badge></Col>
                      <Col xs="2"><Badge>Price Min</Badge></Col>
                      <Col xs="2"><Badge>Price High</Badge></Col>
                      <Col xs="2"><Badge>Price Start</Badge></Col>
                      <Col xs="2"><Badge>Time Start</Badge></Col>
                    </Row>
                  </ListGroupItem>

                  {
                    dateObj.items.map((item, index) => {

                      const renderValue = (key) => {
                        const className = this.upOrDown(item, dateObj.items[index+1], key);
                        return (
                          <Col xs="2" className={className} >
                            {item.price_close}
                          </Col>
                        )
                      }

                      return (
                        <ListGroupItem key={index}>
                          <Row>
                            <Col xs="2">{dateFormat(item.time_period_end, "h:MM TT", true)}</Col>
                            { renderValue("price_close") }
                            { renderValue("price_low") }
                            { renderValue("price_high") }
                            { renderValue("price_open") }
                            <Col xs="2">{dateFormat(item.time_period_start, "h:MM TT", true)}</Col>
                          </Row>
                        </ListGroupItem>
                      )
                    })
                  }

                </ListGroup>
              )
            }) 
  }

}