
const mockData = require("../__mock__/coinapi/get")

export const get = (symbol = "COINBASE_SPOT_BTC_USD", period = "1HRS") => {
  
  const method = "GET";
  const headers = {
    "X-CoinAPI-Key": "28386649-B78D-48DE-A633-53683DE3FD2B"
  }
  const url = `https://rest.coinapi.io/v1/ohlcv/${symbol}/latest?period_id=${period}&limit=100`

 

  return fetch(url, { method, headers })
          .then(response => {
            if (response.status === 429)
              return flipMockData(symbol) // Work around once you hit api limit
            
            return response.json()
          })

}

const flipMockData = (symbol) => {
  if (symbol == "COINBASE_SPOT_BTC_USD")
    return Promise.resolve([...mockData]);
  else
    return Promise.resolve([ ...mockData.reverse() ]);
}